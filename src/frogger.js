"use strict";

var Utils = {
	getTime: function () {
		var date = new Date();
		return (date.getTime() * 0.01) | 0;
	},
	getImage: function (url, callback, error) {
		var image = new Image();
		image.addEventListener('load', function () {
			callback(image);
		});
		image.addEventListener('error', function () {
			error(url);
		});
		image.src = url;
	},
};

function Sprite(image, x, y, flip) {
	this.image = image;
	this.x = x;
	this.y = y;
	this.flip = flip? true : false;
}
Sprite.prototype.draw = function (ctx, x, y) {
	if (this.flip)
		ctx.scale(-1, 1);

	ctx.drawImage(this.image,
			this.x, this.y, 16, 16,
			this.flip? -(x + 16) : x, y, 16, 16);

	if (this.flip)
		ctx.scale(-1, 1);
};

function AnimatedSprite(image, x, y, frames, flip) {
	this.sprite = new Sprite(image, x, y, flip);
	this.frames = frames;
	this.offset = 0;
}
AnimatedSprite.prototype.setOffset = function (offset) {
	this.offset = offset;
};
AnimatedSprite.prototype.draw = function (ctx, x, y, frame) {
	frame = frame | 0;
	var offset =  16 * ((frame - this.offset) % this.frames);

	if (this.sprite.flip)
		ctx.scale(-1, 1);

	ctx.drawImage(this.sprite.image,
			this.sprite.x + offset, this.sprite.y, 16, 16,
			this.sprite.flip? -(x + 16) : x, y, 16, 16);

	if (this.sprite.flip)
		ctx.scale(-1, 1);
};

function FroggerGame(ctx) {
	var sprites = {};
	var current_level;
	var MOVE_UP = 1, MOVE_DOWN = 2, MOVE_LEFT = 4, MOVE_RIGHT = 8;

	function Lane(velocity, sprites) {
		this.sprites = sprites;
		this.velocity = velocity;
		this.offset = 0;
	}
	Lane.prototype.getOffset = function (time) {
		return this.offset + velocity * time;
	};
	Lane.prototype.draw = function (time) {};

	function Player() {
		this.sprite = sprites.playerDownIdle;

		this.direction = MOVE_DOWN;
		this.floor = null;
		this.canMove = true;

		this.x = 104;
		this.y = 224;
		this.dx = 104;
		this.dy = 224;
	}
	Player.prototype.setFloor = function (floor) {
		this.floor = floor;
	};
	Player.prototype.idle = function(time) {
		this.canMove = true;

		switch (this.direction) {
		case MOVE_UP:
			this.sprite = sprites.playerUpIdle;
			break;
		case MOVE_DOWN:
			this.sprite = sprites.playerDownIdle;
			break;
		case MOVE_LEFT:
			this.sprite = sprites.playerLeftIdle;
			break;
		case MOVE_RIGHT:
			this.sprite = sprites.playerRightIdle;
			break;
		}
		this.sprite.setOffset(time);
	};
	Player.prototype.move = function (direction, time) {
		if (!this.canMove)
			return;

		this.canMove = false;
		this.direction = direction;

		switch (direction) {
		case MOVE_UP:
			this.sprite = sprites.playerUp;
			this.dy -= 16;
			break;
		case MOVE_DOWN:
			this.sprite = sprites.playerDown;
			this.dy += 16;
			break;
		case MOVE_LEFT:
			this.sprite = sprites.playerLeft;
			this.dx -= 16;
			break;
		case MOVE_RIGHT:
			this.sprite = sprites.playerRight;
			this.dx += 16;
			break;
		}
		this.sprite.setOffset(time);

		/* Clamp */
		this.dx = Math.min(Math.max(this.dx, 0), 208);
		this.dy = Math.min(Math.max(this.dy, 0), 240);

		setTimeout(this.idle.bind(this), 200, time + 2);
	};
	Player.prototype.die = function(time) {
		this.sprite = sprites.playerDie;
		this.sprite.setOffset(time);
	};
	Player.prototype.draw = function (time) {
		this.x += ((this.dx - this.x) * 0.5) | 0;
		this.y += ((this.dy - this.y) * 0.5) | 0;

		var offset = this.floor? this.floor.getOffset() : 0;
		this.sprite.draw(ctx, this.x + offset, this.y - 4, time);
	};
	Player.prototype.process = function (time) {
		if (this.floor) {
			// TODO: Offscreen die check.
		}
	};

	function Level(lanes) {
		if (lanes.length != 10)
			throw new Error('Levels must have 10 lanes, got ' + lanes.lenght + '.');

		this.lanes = lanes;
		this.player = new Player();
		this.timeStart = Utils.getTime();
	}
	Level.prototype.getTime = function () {
		return Utils.getTime() - this.timeStart;
	};
	Level.prototype.playerMove = function (direction) {
		var time = this.getTime();

		this.player.move(direction, time);
	};
	Level.prototype.draw = function () {
		var time = this.getTime();

		this.player.draw(time);
	};
	Level.prototype.process = function () {
		this.player.process();
	};

	function onKeyDown(event) {
		var input;

		switch (event.keyCode) {
		case 87:
			input = MOVE_UP;
			break;
		case 83:
			input = MOVE_DOWN;
			break;
		case 65:
			input = MOVE_LEFT;
			break;
		case 68:
			input = MOVE_RIGHT;
			break;
		default:
			return;
			break;
		}
		current_level.playerMove(input);
	}
	function draw() {
		ctx.drawImage(sprites.background, 0, 0);
		current_level.draw();

		window.requestAnimationFrame(draw);
	}
	function process() {
		current_level.process();

		setTimeout(process, 200);
	}

	function startOnSpritesReady() {
		if (!sprites.background || !sprites.entity || !sprites.frogger)
			return;

		current_level = new Level([
			new Lane(0),
			new Lane(0),
			new Lane(0),
			new Lane(0),
			new Lane(0),
			new Lane(0),
			new Lane(0),
			new Lane(0),
			new Lane(0),
			new Lane(0),
		]);

		document.addEventListener('keydown', onKeyDown);
		window.requestAnimationFrame(draw);
		setTimeout(process, 500);
	}
	function logOnSpriteError(url) {
		console.error('Faild to load sprite: ' + url);
	}

	Utils.getImage('SpritesSheet_Tiles.png', function (image) {
		var bg = document.createElement('canvas');
		bg.width = 224;
		bg.height = 256;

		var bg_ctx = bg.getContext('2d');
		bg_ctx.fillRect(0, 0, 224, 256);

		var spriteGrass = new Sprite(image, 0, 0);
		var spriteWater = new Sprite(image, 16, 16);
		var spriteWaterBottom = new Sprite(image, 0, 32);
		var spriteRoadTop = new Sprite(image, 0, 48);
		var spriteRoad = new Sprite(image, 0, 64);
		var spriteRoadBottom = new Sprite(image, 0, 80);
		var spriteGrassBottom = new Sprite(image, 0, 96);

		for (var col = 0; col < 224; col += 16) {
			spriteGrass.draw(bg_ctx, col, 16);

			for (var row = 3; row < 7; row++)
				spriteWater.draw(bg_ctx, col, 16 * row);

			spriteWaterBottom.draw(bg_ctx, col, 16 * 7);
			spriteGrass.draw(bg_ctx, col, 16 * 8);
			spriteRoadTop.draw(bg_ctx, col, 16 * 9);

			for (var row = 10; row < 13; row++)
				spriteRoad.draw(bg_ctx, col, 16 * row);

			spriteRoadBottom.draw(bg_ctx, col, 16 * 13);
			spriteGrassBottom.draw(bg_ctx, col, 16 * 14);
		}

		sprites.background = bg;
		startOnSpritesReady();
	}, logOnSpriteError);

	Utils.getImage('SpritesSheet_Entity.png', function (image) {
		sprites.entity = image;

		sprites.bus1 = new Sprite(image, 0, 0);
		sprites.bus2 = new Sprite(image, 16, 0);
		sprites.bus3 = new Sprite(image, 32, 0);

		sprites.carA = new Sprite(image, 48, 0);
		sprites.carB = new Sprite(image, 64, 0);
		sprites.carC = new Sprite(image, 80, 0);

		startOnSpritesReady();
	}, logOnSpriteError);

	Utils.getImage('SpritesSheet_Frogger.png', function (image) {
		sprites.frogger = image;

		sprites.playerDownIdle = new AnimatedSprite(image, 32, 0, 4);
		sprites.playerUpIdle = new AnimatedSprite(image, 32, 16, 4);
		sprites.playerRightIdle = new AnimatedSprite(image, 32, 32, 4);
		sprites.playerLeftIdle = new AnimatedSprite(image, 32, 32, 4, true);

		sprites.playerDown = new AnimatedSprite(image, 0, 0, 6);
		sprites.playerUp = new AnimatedSprite(image, 0, 16, 6);
		sprites.playerRight = new AnimatedSprite(image, 0, 32, 6);
		sprites.playerLeft = new AnimatedSprite(image, 0, 32, 6, true);

		startOnSpritesReady();
	}, logOnSpriteError);
}
